import 'dart:convert';
import 'package:flutter/cupertino.dart';

import '../Widgets/task.dart';
import 'package:localstorage/localstorage.dart';

class TaskManager{
  final LocalStorage storage = LocalStorage('localstorage_app');

  void saveTasks(List<Task> tasks){
    final jsonTasks = jsonEncode(tasks);

    storage.setItem('tasks', jsonTasks);
  }

  Future<List<Task>> loadTasks() async{
    var tempTasks = <Task>[];
    debugPrint("Start of loading");
    await storage.ready;

    String? taskString = storage.getItem('tasks');

    if(taskString != null){

      List<dynamic> data = json.decode(taskString);

      for (var task in data) {
        debugPrint(task.toString());
        Task tempTask = Task(task["title"], task["desc"]);
        tempTask.finished = task["finished"];
        tempTask.deadline = DateTime.tryParse(task["deadline"]);
        tempTask.favourite = task["favourite"];
        tempTask.priority = task["priority"];
        tempTask.late = task["late"];
        tempTasks.add(tempTask);
        debugPrint("task added");
      }


    }

    return orderTasks(tempTasks);
  }

  List<Task> orderTasks(List<Task> tempTasks){
    var tasks = <Task>[];
    var removeTaskIndexes = <int>[];


    for (var task in tempTasks){
      if(task.favourite) {
        tasks.add(task);
        removeTaskIndexes.add(tempTasks.indexOf(task));
      }
    }

    tempTasks = removeTasksByIndex(removeTaskIndexes, tempTasks);
    removeTaskIndexes = <int>[];

    for (var task in tempTasks){
      if(task.priority == "high") {
        tasks.add(task);
        removeTaskIndexes.add(tempTasks.indexOf(task));
      }
    }

    tempTasks = removeTasksByIndex(removeTaskIndexes, tempTasks);
    removeTaskIndexes = <int>[];


    for (var task in tempTasks){
      if(task.priority == "standard") {
        tasks.add(task);
        removeTaskIndexes.add(tempTasks.indexOf(task));
      }
    }

    tempTasks = removeTasksByIndex(removeTaskIndexes, tempTasks);
    removeTaskIndexes = <int>[];

    for (var task in tempTasks){
      tasks.add(task);
    }

    debugPrint("tasks ordered");

    return tasks;
  }

  List<Task> removeTasksByIndex(List<int> indexes, List<Task> tasks){
    for (int index in indexes.reversed){
      tasks.remove(tasks[index]);
    }

    return tasks;
  }

}