import 'package:flutter/material.dart';
import '../Widgets/task.dart';
import 'package:completo/Utilities/save_tasks.dart';

class TaskListProvider extends ChangeNotifier {
  var tasks = <Task>[];
  static TaskManager taskManager = TaskManager();

  void createTask(String title, String desc) {
    tasks.add(Task(title, desc));

    taskManager.saveTasks(tasks);
    notifyListeners();
  }

  void changeTask(String title, String desc, bool fin, int index){

    if(title != "") {
      tasks[index].sender = title;
    }
    if(desc != ""){
      tasks[index].body = desc;
    }

    tasks[index].finished = fin;


    debugPrint(title);
    debugPrint(tasks[index].sender);

    taskManager.saveTasks(tasks);
    tasks = taskManager.orderTasks(tasks);
    notifyListeners();
  }

  void favouriteTask(int index){
    tasks[index].favourite = !tasks[index].favourite;

    taskManager.saveTasks(tasks);
    tasks = taskManager.orderTasks(tasks);
    notifyListeners();
  }

  void deleteTask(int index){
    tasks.remove(tasks[index]);

    taskManager.saveTasks(tasks);
    notifyListeners();
  }

  void changePriority(int index, String? prio){
    tasks[index].priority = prio.toString();

    taskManager.saveTasks(tasks);

    notifyListeners();
  }

  void reorderTasks(){
    tasks = taskManager.orderTasks(tasks);
    notifyListeners();
  }


  void changeDeadline(int index, dynamic date){
    tasks[index].deadline = date;

    taskManager.saveTasks(tasks);
    notifyListeners();
  }

  String convertDate(int index){
    if(tasks[index].deadline == null){
      return "No Date Set";
    }

    if(tasks[index].deadline!.difference(DateTime.now()).inHours <= 0 ) {
      tasks[index].late = true;
    }
    else {
      tasks[index].late = false

      ;
    }

    DateTime date = tasks[index].deadline!;

    String hour = date.hour < 10 ? "0" + date.hour.toString() : date.hour.toString();
    String minute = date.minute < 10 ? "0" + date.minute.toString() : date.minute.toString();


    String timeString = hour + ":" + minute;


    String dateString = date.day.toString() + "." + date.month.toString() + "." + date.year.toString();

    //notifyListeners();
    return dateString + " " + timeString;
  }


  void loadList() async{
    tasks = await taskManager.loadTasks();



    notifyListeners();
  }
}