import 'package:provider/provider.dart';
import '../Providers/tasklist_provider.dart';
import 'package:flutter/material.dart';
import '../Widgets/change_dialog.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class TaskDetail extends StatelessWidget {
  TaskDetail({Key? key,
    required this.index,
    required this.title,
    required this.desc,
    required this.taskProvider})
      : super(key: key);

  final int index;
  final String title;
  final String desc;
  final TaskListProvider taskProvider;
  String text = "";

  @override
  Widget build(BuildContext context) {
    return Consumer<TaskListProvider>(
      builder: (context, taskProvider, child) =>
          SafeArea(
              child: Scaffold(
                body: Align(
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            const SizedBox(height: 25),
                            InkWell(
                              onTap: () {
                                showDialog(
                                    context: context,
                                    builder: (context) {
                                      return ChangeTextDialog(
                                          title: taskProvider.tasks[index]
                                              .getTitle());
                                    }).then(
                                      (text) =>
                                      taskProvider.changeTask(text, "", taskProvider.tasks[index].finished, index),
                                );
                              },
                              child: Text(
                                taskProvider.tasks[index].getTitle(),
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .headline3,
                              ),
                            ),
                            const SizedBox(height: 30),
                            Text(
                              "Description:",
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .subtitle1,
                            ),
                            InkWell(
                              onTap: () {
                                showDialog(
                                    context: context,
                                    builder: (context) {
                                      return ChangeTextDialog(
                                          title: taskProvider.tasks[index]
                                              .getDesc());
                                    }).then(
                                      (text) =>
                                      taskProvider.changeTask("", text,
                                          taskProvider.tasks[index].finished,
                                          index),
                                );
                              },
                              child: Text(
                                taskProvider.tasks[index].getDesc(),
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .bodyText2,
                              ),
                            ),
                            const SizedBox(height: 20),
                            Text(
                              "Priority:",
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .subtitle1,
                            ),
                            DropdownButton<String>(
                                value: taskProvider.tasks[index].priority,
                                onChanged: (String? newVal) {
                                  taskProvider.changePriority(index, newVal);
                                },
                                items: const [
                                  DropdownMenuItem(
                                    child: Text("High"),
                                    value: "high",
                                  ),
                                  DropdownMenuItem(
                                    child: Text("Standard"),
                                    value: "standard",
                                  ),
                                  DropdownMenuItem(
                                    child: Text("Low"),
                                    value: "low",
                                  )
                                ]),
                            const SizedBox(height: 20),
                            Text(
                              "Deadline:",
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .subtitle1,
                            ),
                            InkWell(
                              onTap: () {
                                DatePicker.showDateTimePicker(context,
                                    showTitleActions: true,
                                    minTime: DateTime.now(),
                                    // onChanged: (date) {
                                    //   print('change $date');
                                    // },
                                    onConfirm: (date) {
                                      taskProvider.changeDeadline(index, date);
                                    },
                                    currentTime: DateTime.now(),
                                    locale: LocaleType.en);


                                // showDialog(
                                //     context: context,
                                //     builder: (context) {
                                //       return Dialog(
                                //           child:
                                //
                                //
                                //           SfDateRangePicker(
                                //         onSubmit: (Object value) {
                                //           taskProvider.changeDeadline(index, value);
                                //           Navigator.pop(context);
                                //         },
                                //         onCancel: () {
                                //           Navigator.pop(context);
                                //         },
                                //         selectionMode:
                                //             DateRangePickerSelectionMode.single,
                                //         showActionButtons: true,
                                //       )
                                //       );
                                //     });
                              },
                              child: Text(
                                taskProvider.tasks[index].deadline == null
                                    ? "No Deadline set"
                                    : taskProvider.convertDate(index),
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .bodyText2,
                              ),
                            ),
                          ]),
                    )),
                floatingActionButton: FloatingActionButton(
                  onPressed: () {
                    Navigator.pop(context);
                    taskProvider.reorderTasks();
                  },
                  child: const Icon(Icons.arrow_back),
                ),
              )),
    );
  }
}
