import 'package:completo/Screens/task_detail.dart';
import 'package:flutter/material.dart';
import '../Providers/tasklist_provider.dart';
import 'package:provider/provider.dart';

class TaskList extends StatefulWidget {
  @override
  _TaskListState createState() => _TaskListState();
}

class _TaskListState extends State<TaskList> {
  @override
  initState() {
    super.initState();

    Provider.of<TaskListProvider>(context, listen: false).loadList();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<TaskListProvider>(
      builder: (context, taskProvider, child) => Scaffold(
        body: ListView.builder(
          itemCount: taskProvider.tasks.length,
          itemBuilder: (context, index) {
            final item = taskProvider.tasks[index];

            return Card(
              elevation: 6,
              margin: const EdgeInsets.all(10),
              child: ListTile(
                  title: item.buildTitle(context),
                  subtitle:
                      Text("Deadline: " + taskProvider.convertDate(index)),
                  tileColor: item.finished
                      ? Colors.grey
                      : item.late
                          ? Colors.red
                          : item.favourite
                              ? Colors.orangeAccent
                              : null,
                  leading: Checkbox(
                    value: item.finished,
                    onChanged: (newValue) {
                      item.finished = newValue!;

                      taskProvider.changeTask("", "", newValue, index);
                    },
                  ),
                  trailing: Wrap(spacing: 12, children: <Widget>[
                    PopupMenuButton<String>(
                      onSelected: (String value){
                        switch (value) {
                          case 'Show Details':
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => TaskDetail(
                                        index: index,
                                        title: item.getTitle(),
                                        desc: item.getDesc(),
                                        taskProvider: taskProvider)));
                            break;
                          case 'Toggle Favourite':
                              taskProvider.favouriteTask(index);
                            break;
                          case 'Delete':
                              taskProvider.deleteTask(index);
                            break;
                        }
                      },
                      itemBuilder: (BuildContext context) {
                        return {'Show Details', 'Toggle Favourite', "Delete"}.map((String choice) {
                          return PopupMenuItem<String>(
                            value: choice,
                            child: Text(choice),
                          );
                        }).toList();
                      },
                    ),
                    // IconButton(
                    //     onPressed: () {
                    //       taskProvider.deleteTask(index);
                    //     },
                    //     icon: const Icon(Icons.delete)),
                    // IconButton(
                    //     onPressed: () {
                    //       taskProvider.favouriteTask(index);
                    //     },
                    //     icon: const Icon(Icons.star)),
                    // IconButton(
                    //     onPressed: () {
                    //       Navigator.push(
                    //           context,
                    //           MaterialPageRoute(
                    //               builder: (context) => TaskDetail(
                    //                   index: index,
                    //                   title: item.getTitle(),
                    //                   desc: item.getDesc(),
                    //                   taskProvider: taskProvider)));
                    //     },
                    //     icon: const Icon(Icons.description)),
                  ])),
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            taskProvider.createTask(
                "Name the task", "Create a new Description");
            final item = taskProvider.tasks.last;
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => TaskDetail(
                      index: taskProvider.tasks.length - 1,
                      title: item.getTitle(),
                      desc: item.getDesc(),
                      taskProvider: taskProvider)),
            );
          },
          tooltip: 'Create ',
          child: const Icon(Icons.add),
        ),
      ),
    );
  }
}
