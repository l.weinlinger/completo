import 'package:flutter/material.dart';

class Task{
  String sender;
  String body;
  String priority = "standard";
  bool finished = false;
  bool favourite = false;
  bool late = false;
  DateTime? deadline = DateTime.tryParse("");


  Task(this.sender, this.body);

  Widget buildTitle(BuildContext context) => Text(sender);

  Widget buildSubtitle(BuildContext context) => Text(body);

  String getTitle() => sender;

  String getDesc() => body;

  Map<String, dynamic> toJson(){
    return {
      "title": sender,
      "desc": body,
      "finished": finished,
      "favourite": favourite,
      "priority": priority,
      "late" : late,
      "deadline": deadline.toString()
    };
  }
}