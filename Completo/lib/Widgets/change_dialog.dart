import 'package:flutter/material.dart';
import '../Providers/tasklist_provider.dart';
import 'package:provider/provider.dart';
import 'package:completo/Screens/task_detail.dart';

class ChangeTextDialog extends StatelessWidget {
  const ChangeTextDialog({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    String text = "";
    return Dialog(
        child: Padding(
            padding: const EdgeInsets.all(5),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                   Text("Edit Text",
                    style: Theme.of(context).textTheme.headline6),
                  TextField(
                    //controller: TextEditingController(text: title),
                    minLines: 1,
                    maxLines: 8,
                    keyboardType: TextInputType.multiline,
                    onChanged: (txt) {
                      text = txt;
                    },
                    decoration: const InputDecoration(
                      hintText: "Create a new Text",
                    ),
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      TextButton(
                        child: const Text('Cancel'),
                        onPressed: () => {
                          Navigator.pop(context,""),
                        },
                      ),
                      TextButton(
                        child: const Text('Ok'),
                        onPressed: () => {
                          Navigator.pop(context,text),
                        },
                      )
                    ],
                  ),

                ])));
  }
}
